#dockerfile

FROM python:slim
WORKDIR /app
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
RUN python3 create_db.py
EXPOSE 5000
CMD ["python3", "-m", "flask", "--app", "project/app.py", "run", "--host=0.0.0.0"]
